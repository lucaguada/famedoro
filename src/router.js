import Vue from 'vue'
import Router from 'vue-router'
import Dolomiti from './components/Dolomiti'
import DolomitiEvents from './components/DolomitiEvents'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      components: {
        default: Dolomiti,
        events: DolomitiEvents
      }
    }
  ]
})
